<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Destination extends Model
{
    protected $table    = 'destination';
    protected $guarded  = ['idDestination'];

    public function getColumns(){
        return $this->getConnection()->getSchemaBuilder()->getColumnsListing($this->getTable());
    }

    public function scopeByDesc($query, $values){
        return $query->orderBy($values, 'DESC');
    }

    public function scopeActive($query){
        return $query->where('statedestination', '=', 1);
    }

    public function scopeJoinWithProvinsi($query){
        return $query->join('master_provinsi', 'master_provinsi.provinsi_id', 'destination.provinsi_id');
    }
}
