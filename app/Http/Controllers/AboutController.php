<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AboutController extends Controller
{
    function __construct(){
        date_default_timezone_set("Asia/Jakarta");
        $this->date_now  = date("Y-m-d H:i:s");
    }

    public function index(){
        $locale       = empty(Session("locale")) ? "id" : Session("locale");
        $dataInfoApps = infoApps('AboutMe', $locale);

        return view('about.about', compact('dataInfoApps'));
	}

}
