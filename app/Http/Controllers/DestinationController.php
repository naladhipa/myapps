<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class DestinationController extends Controller
{
    function __construct(){
        date_default_timezone_set("Asia/Jakarta");
        $this->date_now  = date("Y-m-d H:i:s");
    }

    public function DetailDestination($id){
        $locale          = empty(Session("locale")) ? "id" : Session("locale");
        
        $selectadventure = 'destination.destination'.$locale.' as destination, destination.created_at, destination.idDestination, master_provinsi.provinsi_nama, master_provinsi.provinsi_id, destination.picture, destination.info'.$locale.' as info';
        
        $cekDataDestination  = DB::table('destination')
                                ->selectRaw($selectadventure)
                                ->join('master_provinsi', 'master_provinsi.provinsi_id', '=', 'destination.provinsi_id')
                                ->join('category_adventure', 'category_adventure.idCategory', '=', 'destination.idCategory')
                                ->where([['destination.idDestination', '=', $id]])
                                ->first();
        
        $createdData = date('d F Y', strtotime($cekDataDestination->created_at));
        
        $cekDestinationProvinsi = DB::table('destination')
                                    ->selectRaw($selectadventure)
                                    ->join('master_provinsi', 'master_provinsi.provinsi_id', '=', 'destination.provinsi_id')
                                    ->join('category_adventure', 'category_adventure.idCategory', '=', 'destination.idCategory')
                                    ->where([['destination.provinsi_id', '=', $cekDataDestination->provinsi_id]])
                                    ->take('5')
                                    ->orderBy('destination.created_at', 'DESC')
                                    ->get();
        foreach($cekDestinationProvinsi as $c){
            $c->idDestination = GetEncrypt($c->idDestination);
        }
        return view('destination.detaildestination', compact('cekDataDestination', 'cekDestinationProvinsi', 'createdData'));
	}

}

