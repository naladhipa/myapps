<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;
use App\{backgroundbanner, Destination, provinsi, categoryDestination, comment, guide_adventure};
use DB;

class HomeController extends Controller
{
    function __construct(){
        date_default_timezone_set("Asia/Jakarta");
        $this->date_now  = date("Y-m-d H:i:s");
        $this->locale          = empty(Session("locale")) ? "en" : Session("locale");
        $this->selectcategory  = 'Category'.$this->locale.' as Category, idCategory';
        $this->selectcekData   = 'destination'.$this->locale.' as destination, destination.idDestination, destination.visitor, destination.linkdetail, destination.picture, destination.like, master_provinsi.provinsi_nama';
    }

    public function index(){

        $backgroundbanner  = backgroundbanner::active()->bydesc('id')->first();
        $categoryAdventure = categoryDestination::selectRaw($this->selectcategory)->active()->byasc('Category'.$this->locale)->get();
        
        $provinsi  = provinsi::byasc('provinsi_nama')->get();
        $cekData   = Destination::selectRaw($this->selectcekData)->active()->joinwithprovinsi()->take('4')->bydesc('visitor')->get(); 

        $comments  = comment::bydesc('created_at')->get();
        $dataGuide = guide_adventure::active()->take('4')->joinwithprovinsi()->bydesc('likes')->get();
        $provinsi  = provinsi::byasc('provinsi_nama')->get();

        return view('home.home', compact('backgroundbanner', 'categoryAdventure', 'provinsi', 'cekData', 'comments', 'dataGuide'));

	}

}

