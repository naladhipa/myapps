<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
use DB;

class SendEmailController extends Controller
{
    function __construct(){
        date_default_timezone_set("Asia/Jakarta");
        $this->date_now  = date("Y-m-d H:i:s");
        $this->locale    = empty(Session("locale")) ? "en" : Session("locale");
    }

    public function send(Request $request){

        $namalengkap = $request->namalengkap;
        $email       = $request->email;
        $password    = $request->password;
        $pesan       = "registrasi Naladhipa Adventure";
        
        try{
            Mail::send('Mail/isiemail', array('pesan' => $request->namalengkap) , function($pesan) use($request){
                $pesan->to($request->email,'Verifikasi')->subject('Verifikasi Email');
                $pesan->from(env('MAIL_USERNAME','didikprab@gmail.com'),'Verifikasi Akun email anda');
            });
        }catch (Exception $e){
            return response (['status' => false,'errors' => $e->getMessage()]);
        }
    }
}
