<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class backgroundbanner extends Model
{
    protected $table = 'backgroundbanner';
    protected $primaryKey = 'id';
    
    protected $guarded = [
        'created_at',
        'updated_at'
    ];

    public function getColumns() {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }

    public function scopeByDesc($query, $values){
        return $query->orderBy($values, 'DESC');
    }

    public function scopeActive($query){
        return $query->where('statebanner', '=', 1);
    }
}
