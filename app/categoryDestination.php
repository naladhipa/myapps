<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class categoryDestination extends Model
{
    protected $table = 'category_adventure';
    protected $primaryKey = 'idCategory';

    public function getColumns() {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }

    public function scopeByDesc($query, $values){
        return $query->orderBy($values, 'DESC');
    }

    public function scopeByAsc($query, $values){
        return $query->orderBy($values, 'ASC');
    }

    public function scopeActive($query){
        return $query->where('statecategory', '=', 1);
    }
}
