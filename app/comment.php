<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class comment extends Model
{
    protected $table    = 'comment_destination';
    protected $guarded  = ['idComment'];

    public function getColumns(){
        return $this->getConnection()->getSchemaBuilder()->getColumnsListing($this->getTable());
    }

    public function scopeByDesc($query, $values){
        return $query->orderBy($values, 'DESC');
    }
}
