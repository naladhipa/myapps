<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Session;

class footer extends Model
{
    protected $table = 'menu';
    protected $primery_key = 'idMenu';

    public function getfooter(){
        $locale = empty(Session("locale"))?"en":Session("locale");
        $select = 'subtitle'.$locale.' as subtitle, info'.$locale.' as info, title';
        $footer = DB::table('footer_info')->selectRaw($select)->first();
        return $footer;
    }

    public function getTags(){
        $locale = empty(Session("locale"))?"en":Session("locale");
        $select = 'tags'.$locale.' as tags';
        $footer = DB::table('tags')->selectRaw($select)->where('statetags', '=', 1)->get();
        return $footer;
    }

    public function getContactUs(){
        $footer = DB::table('contactus')->first();
        return $footer;
    }
}
