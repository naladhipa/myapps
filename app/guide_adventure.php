<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class guide_adventure extends Model
{
    protected $table = 'guide_adventure';
    protected $primaryKey = 'idGuide';
    
    protected $guarded = [
        'created_at',
        'updated_at'
    ];

    public function getColumns() {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }

    public function scopeActive($query){
        return $query->where('stateguide', '=', 1);
    }

    public function scopeByDesc($query, $values){
        return $query->orderBy($values, 'DESC');
    }

    public function scopeJoinWithProvinsi($query){
        return $query->join('master_provinsi', 'master_provinsi.provinsi_id', 'guide_adventure.provinsi_id');
    }
}
