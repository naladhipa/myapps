<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Session;

class language extends Model
{
    protected $table = 'language';
    protected $primery_key = 'id';

    public function getLanguage(){
        $Language = DB::table('language')->get();
        return $Language;
    }
}
