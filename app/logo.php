<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Session;

class logo extends Model
{
    protected $table = 'logo';
    protected $primery_key = 'id';

    public function getLogo(){
        $Logo = DB::table('logo')->where('statelogo', '=', 1)->first();
        return $Logo;
    }
}
