<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Session;

class menu extends Model
{
    protected $table = 'menu';
    protected $primery_key = 'idMenu';

    public function getMenu(){
        $locale = empty(Session("locale"))?"en":Session("locale");
        $select = 'menu'.$locale.' as Menu, linkMenu';
        $Menu = DB::table('menu')->selectRaw($select)->where('stateMenu', '=', 1)->get();
        return $Menu;
    }
}
