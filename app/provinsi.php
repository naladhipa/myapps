<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class provinsi extends Model
{
    protected $table = 'master_provinsi';
    protected $primaryKey = 'provinsi_id';

    public function getColumns(){
        return $this->getConnection()->getSchemaBuilder()->getColumnsListing($this->getTable());
    }

    public function scopeByAsc($query, $values){
        return $query->orderBy($values, 'ASC');
    }
}
