<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDestination extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('destination', function (Blueprint $table) {
            $table->bigIncrements('idDestination');
            $table->bigInteger('provinsi_id');
            $table->bigInteger('idCategory');
            $table->string('destinationid');
            $table->string('destinationen');
            $table->string('picture');
            $table->string('infoid');
            $table->string('infoen');
            $table->bigInteger('visitor');
            $table->bigInteger('like');
            $table->integer('statedestination');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('destination');
    }
}
