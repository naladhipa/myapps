<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGuideAdventure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('guide_adventure', function (Blueprint $table) {
            $table->bigIncrements('idGuide');
            $table->bigInteger('idDestination');
            $table->string('namalengkap');
            $table->string('gender');
            $table->string('email');
            $table->string('handphone');
            $table->string('identitastype');
            $table->string('identitasno');
            $table->date('birthdate');
            $table->string('pictselfie');
            $table->text('address');
            $table->integer('stateguide');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('guide_adventure');
    }
}
