/*
SQLyog Ultimate v12.5.1 (32 bit)
MySQL - 10.4.8-MariaDB : Database - attract
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`attract` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `attract`;

/*Table structure for table `backgroundbanner` */

DROP TABLE IF EXISTS `backgroundbanner`;

CREATE TABLE `backgroundbanner` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `background` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `statebanner` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `statebanner` (`statebanner`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `backgroundbanner` */

insert  into `backgroundbanner`(`id`,`background`,`statebanner`,`created_at`,`updated_at`) values 
(1,'Gunung-Bromo-Jawa-Timur.jpg',1,'2019-12-02 11:04:42',NULL),
(2,'Pantai-Parangtritis.jpg',1,'2019-12-02 11:04:42',NULL);

/*Table structure for table `category_adventure` */

DROP TABLE IF EXISTS `category_adventure`;

CREATE TABLE `category_adventure` (
  `idCategory` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `Categoryid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Categoryen` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `statecategory` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idCategory`),
  KEY `statecategory` (`statecategory`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `category_adventure` */

insert  into `category_adventure`(`idCategory`,`Categoryid`,`Categoryen`,`statecategory`,`created_at`,`updated_at`) values 
(1,'Candi','Temple',1,'2019-12-03 01:09:59',NULL),
(2,'Gunung','Mountain',1,'2019-12-03 01:10:13',NULL),
(3,'Pantai','Beach',1,'2019-12-03 01:10:30',NULL),
(4,'Menyelam','Diving',1,'2019-12-03 01:10:51',NULL);

/*Table structure for table `comment_destination` */

DROP TABLE IF EXISTS `comment_destination`;

CREATE TABLE `comment_destination` (
  `idComment` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `idDestination` bigint(20) NOT NULL,
  `namalengkap` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idComment`),
  KEY `idDestination` (`idDestination`),
  KEY `idComment` (`idComment`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `comment_destination` */

insert  into `comment_destination`(`idComment`,`idDestination`,`namalengkap`,`comment`,`created_at`,`updated_at`) values 
(1,1,'Oki Pralambang','graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Cicero\'s De Finibus Bonorum et Malorum for use in a type specimen book.\r\n','2019-12-13 00:54:20',NULL),
(4,13,'Naladhipa','graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Cicero\'s De Finibus Bonorum et Malorum for use in a type specimen book.\r\n','2019-12-13 00:54:38',NULL);

/*Table structure for table `destination` */

DROP TABLE IF EXISTS `destination`;

CREATE TABLE `destination` (
  `idDestination` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `provinsi_id` bigint(20) NOT NULL,
  `idCategory` bigint(20) NOT NULL,
  `destinationid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `destinationen` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `picture` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `infoid` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `infoen` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `visitor` bigint(20) NOT NULL,
  `like` bigint(20) NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `latitude` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `longitude` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `linkdetail` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `statedestination` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idDestination`),
  KEY `provinsi_id` (`provinsi_id`),
  KEY `idCategory` (`idCategory`),
  KEY `statedestination` (`statedestination`),
  KEY `visitor` (`visitor`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `destination` */

insert  into `destination`(`idDestination`,`provinsi_id`,`idCategory`,`destinationid`,`destinationen`,`picture`,`infoid`,`infoen`,`visitor`,`like`,`address`,`latitude`,`longitude`,`linkdetail`,`statedestination`,`created_at`,`updated_at`) values 
(1,15,1,'Candi Borobudur','Borobudur Temple','12.jpg','Dinasti Sailendra membangun monumen Buddha terbesar di dunia antara tahun 780 dan 840 M. Sailendra adalah dinasti yang berkuasa di Jawa Tengah pada saat itu. Itu dibangun sebagai tempat untuk memuliakan Buddha dan tempat ziarah untuk membimbing umat manusia dari keinginan duniawi menuju pencerahan dan kebijaksanaan menurut Buddha. Monumen ini ditemukan oleh Inggris pada tahun 1814 di bawah Sir Thomas Stanford Raffles, hingga tahun 1835 seluruh area candi telah dibersihkan.\r\n\r\nBorobudur dibangun dengan gaya Mandala yang melambangkan alam semesta dalam ajaran Buddha. Struktur ini berbentuk persegi dengan empat titik masuk dan titik tengah melingkar. Bekerja dari eksterior ke interior, tiga zona kesadaran diwakili, dengan bola pusat mewakili ketidaksadaran atau Nirvana.','The Sailendra dynasty built this Largest Buddhist monument in the world between AD 780 and 840. The Sailendra are the ruling dynasty in Central Java at the time. It was built as a place for glorifying Buddha and a pilgrimage spot to guide mankind from worldly desires into enlightenment and wisdom according to Buddha. This monument was discovered by the British in 1814 under Sir Thomas Stanford Raffles, it was until 1835 that the entire area of the temple has been cleared.\r\n\r\nBorobudur built in the style of Mandala which symbolizes the universe in Buddhist teaching. This structure is square shaped with four entry point and a circular center point. Working from the exterior to the interior, three zones of consciousness are represented, with the central sphere representing unconsciousness or Nirvana.',11,11,'','','','fb75730d231668e7bcfcb8a7a58c9c3dcd06b5f4c194e8a3ef1a2e98383b9668',1,'2019-12-03 12:16:06',NULL),
(2,16,2,'Gunung Semeru','Semeru Mountain','13.jpg','Gunung Semeru atau Gunung Meru adalah sebuah gunung berapi kerucut di Jawa Timur, Indonesia. Gunung Semeru merupakan gunung tertinggi di Pulau Jawa, dengan puncaknya Mahameru, 3.676 meter dari permukaan laut (mdpl). Gunung Semeru juga merupakan gunung berapi tertinggi ketiga di Indonesia setelah Gunung Kerinci di Sumatra dan Gunung Rinjani di Nusa Tenggara Barat[1]. Kawah di puncak Gunung Semeru dikenal dengan nama Jonggring Saloko. Gunung Semeru secara administratif termasuk dalam wilayah dua kabupaten, yakni Kabupaten Malang dan Kabupaten Lumajang, Provinsi Jawa Timur. Gunung ini termasuk dalam kawasan Taman Nasional Bromo Tengger Semeru.\r\n\r\nSemeru mempunyai kawasan hutan Dipterokarp Bukit, hutan Dipterokarp Atas, hutan Montane, dan Hutan Ericaceous atau hutan gunung.\r\n\r\nPosisi geografis Semeru terletak antara 8°06\' LS dan 112°55\' BT.\r\n\r\nPada tahun 1913 dan 1946 Kawah Jonggring Saloka memiliki kubah dengan ketinggian 3.744,8 m hingga akhir November 1973. Di sebelah selatan, kubah ini mendobrak tepi kawah menyebabkan aliran lava mengarah ke sisi selatan meliputi daerah Pronojiwo dan Candipuro di Lumajang.','Mount Semeru or Mount Meru is a cone volcano in East Java, Indonesia. Mount Semeru is the highest mountain on the island of Java, with its peak Mahameru, 3,676 meters above sea level (masl). Mount Semeru is also the third highest volcano in Indonesia after Mount Kerinci in Sumatra and Mount Rinjani in West Nusa Tenggara [1]. The crater at the top of Mount Semeru is known by the name Jonggring Saloko. Mount Semeru is administratively included in the area of ​​two districts, namely Malang Regency and Lumajang Regency, East Java Province. This mountain is included in the Bromo Tengger Semeru National Park area.\r\n\r\nSemeru has a Bukit Dipterocarp forest area, Upper Dipterocarp forest, Montane forest, and Ericaceous Forest or mountain forest.\r\n\r\nSemeru\'s geographical position is located between 8 ° 06 \'latitude and 112 ° 55\' east.\r\n\r\nIn 1913 and 1946 the Jonggring Saloka Crater had a dome with a height of 3,744.8 m until the end of November 1973. In the south, this dome broke through the crater rim causing lava flow to the south covering the Pronojiwo and Candipuro areas in Lumajang.',15,15,'','','','be7a0d96dd71a9ea7e83ca2b1475c844059f9add79258a0c293c9925d321fd7e',1,'2019-12-03 12:33:47',NULL),
(3,33,3,'Raja Ampat','Raja Ampat','14.jpg','Kepulauan Raja Ampat merupakan rangkaian empat gugusan pulau yang berdekatan dan berlokasi di barat bagian Kepala Burung (Vogelkoop) Pulau Papua. Secara administrasi, gugusan ini berada di bawah Kabupaten Raja Ampat, Provinsi Papua Barat. Kepulauan ini sekarang menjadi tujuan para penyelam yang tertarik akan keindahan pemandangan bawah lautnya. Empat gugusan pulau yang menjadi anggotanya dinamakan menurut empat pulau terbesarnya, yaitu Pulau Waigeo, Pulau Misool, Pulau Salawati, dan Pulau Batanta.','Located off the northwest tip of Bird\'s Head Peninsula on the island of New Guinea, in Indonesia\'s West Papua province, Raja Ampat, or the Four Kings, is an archipelago comprising over 1,500 small islands, cays, and shoals surrounding the four main islands of Misool, Salawati, Batanta, and Waigeo, and the smaller island of Kofiau. The Raja Ampat archipelago straddles the Equator and forms part of Coral Triangle which contains the richest marine biodiversity on earth.\r\n\r\nAdministratively, the archipelago is part of the province of West Papua (formerly known as Irian Jaya). Most of the islands constitute the Raja Ampat Regency, which was separated out from Sorong Regency in 2004. The regency encompasses around 70,000 square kilometres (27,000 sq mi) of land and sea, and has a population of about 50,000 (as of 2017).',13,13,'','','','73dbf41bfd252825cb28110a36e60a736610199305f804093474ad94352506c6',1,'2019-12-03 13:11:46',NULL),
(4,17,4,'SCUBA DIVING','SCUBA DIVING','15.jpg','apakah Anda seorang penyelam atau ingin belajar? Menyelam di Bali beragam dan beragam. Apakah Anda seorang penyelam berpengalaman atau pemula, Anda akan mendapatkan nilai uang Anda. Kami menawarkan kursus menyelam harian dalam kelompok kecil.\r\n\r\nAnda tertarik secara budaya, dan ingin mengenal negara dan orang-orangnya? Jika demikian, Bali adalah tujuan liburan utama bagi Anda. Izinkan kami untuk membawa Anda, biarkan kami menunjukkan kepada Anda dunia bawah laut Bali yang fantastis dan biarkan Anda terpesona oleh seni dan budaya, desa-desa yang menawan, bukit hijau, gunung berapi, dan pantai-pantai Bali. Terletak di barat laut dan timur laut pulau liburan yang populer, pusat penyelaman dan resor kami dirancang untuk menarik setiap jenis kegiatan rekreasi. Lokasi utama resort kami menawarkan kesempatan untuk menjelajahi seluruh utara, baik di bawah air maupun secara budaya.\r\n\r\nTeks berikut ini mencoba memberi Anda pemahaman tentang daya tarik Bali di atas dan di atas semua di bawah air dan untuk menjawab pertanyaan yang sering diajukan kepada kami.','re you a diver or would you like to learn? Diving in Bali is diverse and varied. Whether you are an experienced diver or a beginner you will get your money’s worth. We offer daily diving courses in small groups.\r\n\r\nYou are culturally interested, and would like to get to know the country and its people? If so, Bali is the ultimate holiday destination for you. Allow us to take you with us, let us show you the fantastic underwater world of Bali and let you be enchanted by art and culture, charming villages, green hills, volcanoes and Bali’s beaches. Situated in the northwest and northeast of the popular holiday island, our dive centers and resorts are designed to appeal to every type of recreational activity. The prime location of our resorts offers the opportunity to explore the entire north, both underwater and culturally.\r\n\r\nThe following text tries to give you an understanding of Bali’s appeal above and above all under water and to answer questions that are frequently put to us.',17,17,'','','','bb7c05b8e67541809ca2cdde6a6803542b80ec0b38cd9513c5d2be3c212c338e',1,'2019-12-03 13:14:33',NULL);

/*Table structure for table `failed_jobs` */

DROP TABLE IF EXISTS `failed_jobs`;

CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `failed_jobs` */

/*Table structure for table `footer_info` */

DROP TABLE IF EXISTS `footer_info`;

CREATE TABLE `footer_info` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subtitleid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subtitleen` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `infoid` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `infoen` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `footer_info` */

insert  into `footer_info`(`id`,`title`,`subtitleid`,`subtitleen`,`infoid`,`infoen`,`created_at`,`updated_at`) values 
(1,'Naladhipa Adventure','PETUNJUK APLIKASI PETUALANGAN INDONESIA','APPLICATION GUIDE ADVENTURE INDONESIA','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer pulvinar sed mauris eget tincidunt. Sed lectus nulla, tempor vel eleifend quis, tempus rut rum metus. Pellentesque ultricies enim eu quam fermentum hendrerit.','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer pulvinar sed mauris eget tincidunt. Sed lectus nulla, tempor vel eleifend quis, tempus rut rum metus. Pellentesque ultricies enim eu quam fermentum hendrerit.','2019-12-15 04:31:11',NULL);

/*Table structure for table `guide_adventure` */

DROP TABLE IF EXISTS `guide_adventure`;

CREATE TABLE `guide_adventure` (
  `idGuide` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `idDestination` bigint(20) NOT NULL,
  `provinsi_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `namalengkap` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `handphone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `identitastype` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `identitasno` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `birthdate` date NOT NULL,
  `pictselfie` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` bigint(255) NOT NULL,
  `likes` bigint(20) NOT NULL,
  `dislikes` bigint(255) NOT NULL,
  `stateguide` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idGuide`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `guide_adventure` */

insert  into `guide_adventure`(`idGuide`,`idDestination`,`provinsi_id`,`namalengkap`,`gender`,`email`,`handphone`,`identitastype`,`identitasno`,`birthdate`,`pictselfie`,`address`,`price`,`likes`,`dislikes`,`stateguide`,`created_at`,`updated_at`) values 
(1,1,'15','Naladhipa Pralambang','LakiLaki','naladhipapralambang@gmail.com','087809496655','KTP','123456789012345','1983-10-28','popular_1.jpg','Sleman, Yogyakarta',200000,1,0,1,'2019-12-13 16:54:12',NULL);

/*Table structure for table `language` */

DROP TABLE IF EXISTS `language`;

CREATE TABLE `language` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `language` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `picture` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `information` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `language` */

insert  into `language`(`id`,`language`,`picture`,`information`,`created_at`,`updated_at`) values 
(1,'id','3.png','Bahasa Indonesia','2019-12-02 03:09:19',NULL),
(2,'en','1.jpg','Bahasa Inggris','2019-12-02 03:09:28',NULL);

/*Table structure for table `logo` */

DROP TABLE IF EXISTS `logo`;

CREATE TABLE `logo` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `infoLogo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `statelogo` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `logo` */

insert  into `logo`(`id`,`logo`,`infoLogo`,`statelogo`,`created_at`,`updated_at`) values 
(1,'logo.png','Attract Adventure',1,'2019-12-02 13:27:07',NULL);

/*Table structure for table `master_provinsi` */

DROP TABLE IF EXISTS `master_provinsi`;

CREATE TABLE `master_provinsi` (
  `provinsi_id` int(10) NOT NULL AUTO_INCREMENT,
  `provinsi_nama` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`provinsi_id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;

/*Data for the table `master_provinsi` */

insert  into `master_provinsi`(`provinsi_id`,`provinsi_nama`) values 
(1,'Nanggroe Aceh Darussalam'),
(2,'Sumatera Utara'),
(3,'Sumatera Barat'),
(4,'Riau'),
(5,'Kepulauan Riau'),
(6,'Kepulauan Bangka-Belitung'),
(7,'Jambi'),
(8,'Bengkulu'),
(9,'Sumatera Selatan'),
(10,'Lampung'),
(11,'Banten'),
(12,'DKI Jakarta'),
(13,'Jawa Barat'),
(14,'Jawa Tengah'),
(15,'Daerah Istimewa Yogyakarta  '),
(16,'Jawa Timur'),
(17,'Bali'),
(18,'Nusa Tenggara Barat'),
(19,'Nusa Tenggara Timur'),
(20,'Kalimantan Barat'),
(21,'Kalimantan Tengah'),
(22,'Kalimantan Selatan'),
(23,'Kalimantan Timur'),
(24,'Gorontalo'),
(25,'Sulawesi Selatan'),
(26,'Sulawesi Tenggara'),
(27,'Sulawesi Tengah'),
(28,'Sulawesi Utara'),
(29,'Sulawesi Barat'),
(30,'Maluku'),
(31,'Maluku Utara'),
(32,'Papua Barat'),
(33,'Papua'),
(34,'Kalimantan Utara');

/*Table structure for table `menu` */

DROP TABLE IF EXISTS `menu`;

CREATE TABLE `menu` (
  `idMenu` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `titleMenu` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `MenuId` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `MenuEn` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `linkMenu` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stateMenu` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idMenu`),
  KEY `stateMenu` (`stateMenu`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `menu` */

insert  into `menu`(`idMenu`,`titleMenu`,`MenuId`,`MenuEn`,`linkMenu`,`stateMenu`,`created_at`,`updated_at`) values 
(1,'Beranda','Beranda','Home','/','1','2019-12-02 03:25:17',NULL),
(2,'Tentang','Tentang Kami','About Me','/AboutMe','0','2019-12-02 03:25:33',NULL),
(3,'Login','Login','Login','#','0','2019-12-14 00:52:17',NULL);

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `migrations` */

insert  into `migrations`(`id`,`migration`,`batch`) values 
(9,'2014_10_12_000000_create_users_table',1),
(10,'2014_10_12_100000_create_password_resets_table',1),
(11,'2019_08_19_000000_create_failed_jobs_table',1),
(12,'2019_12_01_200234_create_language',1),
(14,'2019_12_01_202238_create_menu',2),
(16,'2019_12_02_040027_create_backgroundbanner',3),
(19,'2019_12_02_053256_create_logo',4),
(20,'2019_12_02_081726_create_socialmenu',5),
(21,'2019_12_02_110402_create_destination',6),
(22,'2019_12_02_180814_create_category_adventure',7),
(23,'2019_12_03_051506_create_destination',8),
(24,'2019_12_10_160903_add_latitude_field_to_destination_table',9),
(25,'2019_12_10_161217_add_address_field_to_destination_table',10),
(26,'2019_12_10_161308_add_longitude_field_to_destination_table',11),
(27,'2019_12_10_173101_create__comment_destination',12),
(28,'2019_12_10_173348_create_comment_destination',13),
(29,'2019_12_12_193435_add_linkdetail_field_to_destination_table',14),
(30,'2019_12_13_093521_create_guide_adventure',15),
(31,'2019_12_13_095602_add_price_field_to_guide_adventure_table',16),
(32,'2019_12_13_100737_add_likes_field_to_guide_adventure_table',17),
(33,'2019_12_13_100820_add_dislikes_field_to_guide_adventure_table',18),
(34,'2019_12_13_102209_alter_likes_to_text_on_guide_adventure_table',19),
(35,'2019_12_13_102318_alter_likes_to_guide_adventure_table',20),
(36,'2019_12_14_204954_add_provinsi_id_field_to_guide_adventure_table',20),
(37,'2019_12_14_212851_create_footer_info',21),
(38,'2019_12_14_213810_create_tags',22);

/*Table structure for table `password_resets` */

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `password_resets` */

/*Table structure for table `socialmenu` */

DROP TABLE IF EXISTS `socialmenu`;

CREATE TABLE `socialmenu` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `fontfa` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `statefontfa` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `socialmenu` */

insert  into `socialmenu`(`id`,`fontfa`,`link`,`statefontfa`,`created_at`,`updated_at`) values 
(1,'fa fa-instagram','',1,'2019-12-02 15:18:50',NULL),
(2,'fa fa-facebook','',1,'2019-12-02 15:18:52',NULL);

/*Table structure for table `tags` */

DROP TABLE IF EXISTS `tags`;

CREATE TABLE `tags` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tagsid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tagsen` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `statetags` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `tags` */

insert  into `tags`(`id`,`tagsid`,`tagsen`,`statetags`,`created_at`,`updated_at`) values 
(1,'Perjalanan','Travel',1,'2019-12-15 04:39:24',NULL),
(2,'Petualangan','Adventure',1,'2019-12-15 04:40:06',NULL);

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `users` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
