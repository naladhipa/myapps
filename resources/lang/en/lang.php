<?php

$name = "Naladhipa Adventure";
return [
    'namewebsite'       => $name,
    'home'              => "Home ".$name,
    'AboutMe'           => "About ".$name,
    'DetailDestination' => "Detail Adventure",
    'logintext'         => 'Login '.$name,
    'forgotpass'        => 'Forgot Password ?',
    'register'          => 'Register',
    'fullname'          => 'Full Name',
    'keyword'           => "Keywords",
    'FindAdnventure'    => "Find Your Adventure Place",
    'backendBanner'     => "discover",
    'topBanner'         => $name." INDONESIA",
    'Adventure'         => "Adventure",
    'typeAdventure'     => "Adventure Type",
    'province'          => "Province",
    'date'              => "Date",
    'find'              => "Find",
    'topDestination'    => "Top Destinations Adventure",
    'topguides'         => "Top Adventure Guides",
    'thisHere'          => "people have been here",
    'provindonesia'     => "Choose Your Adventure Province",
    'Newsletter'        => "Subscribe to our Newsletter",
    'infoemail'         => "Your Email Address",
    'subscrice'         => "Subscribe",
];