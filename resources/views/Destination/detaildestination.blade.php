@extends('layout.template')

@section('title', trans('lang.DetailDestination'))

@section('scriptCss')
<link rel="stylesheet" type="text/css" href="{{ url('assets/styles/news_styles.css') }}">
<link rel="stylesheet" type="text/css" href="{{ url('assets/styles/news_responsive.css') }}">
<link rel="stylesheet" type="text/css" href="{{ url('assets/styles/slide.css') }}">
@endsection

@section('main')
<div class="home">
    <div class="home_background parallax-window" data-parallax="scroll" data-image-src="{{ url('assets/images/adventure/'.$cekDataDestination->picture) }}" data-speed="0.8"></div>
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="home_content">
                    <div class="home_content_inner">
                        <div class="home_title"><b>{{ $cekDataDestination->destination }}</b></div>
                        <div class="home_breadcrumbs">
                            <ul class="home_breadcrumbs_list">
                                <li class="home_breadcrumb"><b>{{ $cekDataDestination->provinsi_nama }}</b></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>		
</div>



<div class="news">
    <div class="container">
        <div class="row">
            <div class="col-lg-9">
                <div class="news_posts">
                    <div class="news_post">
                        <div class="post_title"><a href="#"><b>{{ $cekDataDestination->destination }}</b></a></div>
                        <div class="post_meta">
                            <ul>
                                <li><a href="#">{{ $cekDataDestination->provinsi_nama }}</a></li>
                                <li>{{ $createdData }}</li>
                                <li><a href="#">3 comments</a></li>
                            </ul>
                        </div>
                        <div class="post_image">
                            <img src="{{ url('assets/images/adventure/'.$cekDataDestination->picture) }}" style='width:100%'>
                            <a href="#"><div class="post_image_box text-center">+</div></a>
                        </div>
                        <div class="post_text">
                            <p style="text-align:justify;">{{ $cekDataDestination->info }}.</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3">
                <div class="sidebar">
                    <div class="sidebar_search">
                        <input type="search" class="sidebar_search_input" placeholder="Search">
                    </div>

                    <div class="sidebar_featured">
                        @foreach($cekDestinationProvinsi as $cdp)
                            <div class="sidebar_featured_post">
                                <a href="/Destination/DetailDestination/{{$cdp->idDestination}}">
                                    <div class="sidebar_featured_image"><img src="{{ url('assets/images/adventure/'.$cdp->picture) }}" alt=""></div>
                                    <div class="sidebar_featured_title"><b>{{$cdp->destination}}</b></div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="container">
    
    </div>
</div>
@endsection

@section('script')
<script src="{{ url('assets/plugins/greensock/TweenMax.min.js') }}"></script>
<script src="{{ url('assets/plugins/greensock/TimelineMax.min.js') }}"></script>
<script src="{{ url('assets/plugins/scrollmagic/ScrollMagic.min.js') }}"></script>
<script src="{{ url('assets/plugins/greensock/animation.gsap.min.js') }}"></script>
<script src="{{ url('assets/plugins/greensock/ScrollToPlugin.min.js') }}"></script>
@endsection