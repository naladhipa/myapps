@extends('layout.template')

@section('title', trans('lang.home'))

@section('scriptCss')
	<link rel="stylesheet" type="text/css" href="{{ url('assets/plugins/OwlCarousel2-2.2.1/owl.carousel.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ url('assets/plugins/OwlCarousel2-2.2.1/owl.theme.default.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ url('assets/plugins/OwlCarousel2-2.2.1/animate.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ url('assets/plugins/magnific-popup/magnific-popup.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ url('assets/styles/main_styles.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ url('assets/styles/responsive.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ url('assets/styles/style.css') }}">
@endsection

@section('main')
<style>
  	.carousel-inner img {
      	width: 100%;
      	height: 100%;
  	}
</style>
<div class="home">
	@if(!empty($backgroundbanner))
		<div class="home_background" style="background-image:url({{ url('assets/images/banner/'.$backgroundbanner->background) }})"></div>
	@endif
	<div class="home_content">
		<div class="home_content_inner">
			<div class="home_text_large">@lang('lang.backendBanner')</div>
			<div class="home_text_small">@lang('lang.topBanner')</div>
		</div>
	</div>
</div>

<!-- Find Form -->

<div class="find">
	<!-- Image by https://unsplash.com/@garciasaldana_ -->
	<div class="find_background parallax-window" data-parallax="scroll" data-image-src="{{ url('assets/images/find.jpg') }}" data-speed="0.8"></div>
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="find_title text-center">@lang('lang.FindAdnventure') :</div>
			</div>
			<div class="col-12">
				<div class="find_form_container">
					<form action="#" id="find_form" class="find_form d-flex flex-md-row flex-column align-items-md-center align-items-start justify-content-md-between justify-content-start flex-wrap">
						<div class="find_item col-md-3">
							<div>@lang('lang.Adventure'):</div>
							<input type="text" class="destination find_input" required="required" placeholder="@lang('lang.keyword')">
						</div>
						<div class="find_item col-md-3">
							<div>@lang('lang.typeAdventure'):</div>
							<select name="CategoryAdventure" id="adventure" class="dropdown_item_select find_input">
								<option>@lang('lang.typeAdventure')</option>
								@foreach($categoryAdventure as $ca)
									<option value="{{ $ca->idCategory }}">{{ $ca->Category }}</option>
								@endforeach
							</select>
						</div>
						<div class="find_item col-md-3">
							<div>@lang('lang.province'):</div>
							<select name="provinsi" id="provinsi" class="dropdown_item_select find_input">
								<option>@lang('lang.province')</option>
								@foreach($provinsi as $p)
									<option value="{{ $p->provinsi_id }}">{{ $p->provinsi_nama }}</option>
								@endforeach
							</select>
						</div>
						<button class="button find_button">@lang('lang.find')</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Top Destinations -->
<div class="top">
	<div class="container">
		@if(count($cekData) != 0)
			<div class="row">
				<div class="col">
					<div class="section_title text-center">
						<h2>@lang('lang.topDestination')</h2>
					</div>
				</div>
			</div>
			<div class="row top_content">
			@foreach($cekData as $cd)
				<div class="owl-item active col-md-3">
					<a href="#">
						<div class="col-lg-12 col-md-12 top_col" style="display:inline-block;">
							<div class="mgx-auto mgb-1 p-relative bg-white for-shadow" style="box-shadow: 2px 2px 2px rgba(0,0,0,0.8);width: auto; overflow: hidden; border-radius: 4px;border:1px solid #ccc;">
								<img style="height: 212px; object-fit:cover;width:100%" src="{{ url('assets/images/adventure/'.$cd->picture) }}" alt="" class="d-block w-full">
								<div class="pdx-1 pdy-1 line-card line-card" style="padding:10px;">
									<h4 class="eerie-black card-heading ellipsis-text mt-0 mgb-1m">
										{{ $cd->destination }}
									</h4>
									<p class="nmb card-info" style="font-size:13px;">
										<b>{{ $cd->provinsi_nama }}</b>
									</p>
									
									<div class="row">
										<div class="col-9" style="font-size:11px;">
											{{ $cd->visitor }} @lang('lang.thisHere')
										</div>
										<div class="col-3" style="text-align:right;font-size:11px;">
											{{ $cd->like }} <i class="fa fa-thumbs-up" aria-hidden="true"></i>
										</div>
									</div>
								</div>
							</div>
						</div>
					</a>
				</div>
			@endforeach
			</div>
		@endif
	</div>
</div>
<!-- Last -->

<div class="last">
	<div class="last_background parallax-window" data-parallax="scroll" data-image-src="{{ url('assets/images/last.jpg') }}" data-speed="0.8"></div>
	<div class="container">
		<div id="demo" class="carousel slide col-md-8 mt-3" data-ride="carousel" style='margin:auto'>
			<!-- Ganti gambar dengan file gambar kalian -->
			<div class="carousel-inner">
	  			@foreach($comments as $cm)
	  				@if($cm->idComment === 1)
						<div class="carousel-item active">
							<div class="row">
								<div class="col-lg-12 last_col">
									<div class="last_item">
										<div class="last_item_content">
											<div class="last_title">{{ $cm->namalengkap }}</div>
											<div class="last_text">"{{ $cm->comment }}"</div>
										</div>
									</div>
								</div>
							</div>
						</div>
	  				@else
						<div class="carousel-item">
							<div class="row">
								<div class="col-lg-12 last_col">
									<div class="last_item">
										<div class="last_item_content">
											<div class="last_title">{{ $cm->namalengkap }}</div>
											<div class="last_text">"{{ $cm->comment }}"</div>
										</div>
									</div>
								</div>
							</div>
						</div>
	  				@endif
				@endforeach
			</div>

			<a class="carousel-control-prev" href="#demo" data-slide="prev">
				<span class="carousel-control-prev-icon"></span>
			</a>
			<a class="carousel-control-next" href="#demo" data-slide="next">
				<span class="carousel-control-next-icon"></span>
			</a>
		</div>
	</div>
</div>


<!-- Popular -->

<div class="popular">
	<div class="container">
		@if(count($dataGuide) != 0)
			<div class="row">
				<div class="col">
					<div class="section_title text-center">
						<h2>@lang('lang.topguides')</h2>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col">
					<div class="popular_content d-flex flex-md-row flex-column flex-wrap align-items-md-center align-items-start justify-content-md-between justify-content-start">
						
						@foreach($dataGuide as $dg)
							<div class="owl-item active col-md-3">
								<a href="#">
									<div class="col-lg-12 col-md-12 top_col" style="display:inline-block;">
										<div class="mgx-auto mgb-1 p-relative bg-white for-shadow" style="box-shadow: 2px 2px 2px rgba(0,0,0,0.8);width: auto; overflow: hidden; border-radius: 4px;border:1px solid #ccc;">
											<img style="height: 212px; object-fit:cover;width:100%" src="{{ url('assets/images/guide/'.$dg->pictselfie) }}" alt="" class="d-block w-full">
											<div class="pdx-1 pdy-1 line-card line-card" style="padding:10px;">
												<h4 class="eerie-black card-heading ellipsis-text mt-0 mgb-1m" style="font-size:13px;">
													{{ $dg->namalengkap }}
												</h4>
												<p class="nmb card-info" style="font-size:13px;">
													<b>{{ $dg->provinsi_nama }}</b>
												</p>
												
												<div class="row">
													<div class="col-7" style="font-size:11px;">
														@currency($dg->price)
													</div>
													<div class="col-5" style="text-align:right;font-size:11px;">
														{{ $dg->likes }} <i class="fa fa-thumbs-up" aria-hidden="true"></i> &nbsp;&nbsp;
														{{ $dg->dislikes }} <i class="fa fa-thumbs-down" aria-hidden="true"></i>
													</div>
												</div>
											</div>
										</div>
									</div>
								</a>
							</div>
						@endforeach
					</div>
				</div>
			</div>
		@endif
	</div>
</div>

<!-- Special -->

<div class="special">
	<div class="container">
		<div class="row">
			<div class="col">
				<div class="section_title text-center">
					<h2>@lang('lang.provindonesia')</h2>
				</div>
			</div>
		</div>
	</div>
	<div class="special_content">
		<div class="special_slider_container">
			<div class="owl-carousel owl-theme special_slider">
				@foreach($provinsi as $p)
				<div class="owl-item">
					<div class="special_item">
						<div class="special_item_background"><img src="{{ url('assets/images/provinsi/special_1.jpg') }}" alt="https://unsplash.com/@garciasaldana_"></div>
						<div class="special_item_content text-center">
							<div class="special_category">Visiting</div>
							<div class="special_title"><a href="#" style="font-size:15px;">{{ $p->provinsi_nama }}</a></div>
						</div>
					</div>
				</div>
				@endforeach
			</div>

			<div class="special_slider_nav d-flex flex-column align-items-center justify-content-center">
				<img src="{{ url('assets/images/special_slider.png') }}" alt="">
			</div>
		</div>
	</div>
</div>

<!-- Newsletter -->

<div class="newsletter">
	<!-- Image by https://unsplash.com/@garciasaldana_ -->
	<div class="newsletter_background" style="background-image:url({{ url('assets/images/newsletter.jpg') }})"></div>
	<div class="container">
		<div class="row">
			<div class="col-lg-10 offset-lg-1">
				<div class="newsletter_content">
					<div class="newsletter_title text-center">@lang('lang.Newsletter')</div>
					<div class="newsletter_form_container">
						<form action="#" id="newsletter_form" class="newsletter_form">
							@csrf
							<div class="d-flex flex-md-row flex-column align-content-center justify-content-between">
								<input type="email" id="newsletter_input" class="newsletter_input" placeholder="@lang('lang.infoemail')">
								<button type="submit" id="newsletter_button" class="newsletter_button">@lang('lang.subscrice')</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@section('script')
	<script src="{{ url('assets/plugins/OwlCarousel2-2.2.1/owl.carousel.js') }}"></script>
@endsection