<?php
    $footer  = new \App\footer();
    $footer_ = $footer->getfooter();
    $tags_   = $footer->getTags();
    $contactus_   = $footer->getContactUs();
?>
<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 footer_col">
                <div class="footer_about">
                    <div class="logo_container">
                        <div class="logo">
                            <div>{{ $footer_->title }}</div>
                            <div>{{ $footer_->subtitle }}</div>
                            <div class="logo_image"><img src="{{ url('assets/images/logo.png') }}" alt=""></div>
                        </div>
                    </div>
                    <div class="footer_about_text">
                        {{ $footer_->info }}
                    </div>
                    <div class="copyright">
                        Copyright &copy;
                        <script>
                            document.write(new Date().getFullYear());
                        </script> 
                        All rights reserved | This template is made with @lang('lang.namewebsite')
                    </div>
                </div>
            </div>

            
            <!-- 
            <div class="col-lg-4 footer_col">
                <div class="footer_latest">
                    <div class="footer_title">Latest News</div>
                    <div class="footer_latest_content">
                        <div class="footer_latest_item">
                            <div class="footer_latest_image"><img src="{{ url('assets/images/latest_1.jpg') }}" alt="https://unsplash.com/@peecho"></div>
                            <div class="footer_latest_item_content">
                                <div class="footer_latest_item_title"><a href="news.html">Brazil Summer</a></div>
                                <div class="footer_latest_item_date">Jan 09, 2018</div>
                            </div>
                        </div>
                        <div class="footer_latest_item">
                            <div class="footer_latest_image"><img src="{{ url('assets/images/latest_2.jpg') }}" alt="https://unsplash.com/@sanfrancisco"></div>
                            <div class="footer_latest_item_content">
                                <div class="footer_latest_item_title"><a href="news.html">A perfect vacation</a></div>
                                <div class="footer_latest_item_date">Jan 09, 2018</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->

            <!-- Footer Column -->
            <div class="col-lg-4 footer_col">
                <div class="tags footer_tags">
                    <div class="footer_title">Tags</div>
                    <ul class="tags_content d-flex flex-row flex-wrap align-items-start justify-content-start">
                        @foreach($tags_ as $tg)
                            <li class="tag"><a href='#'>{{ $tg->tags }}</a></li>
                        @endforeach
                    </ul>
                </div>
            </div>

             
            <div class="col-lg-4 footer_col">
                <div class="footer_latest">
                    <div class="footer_title">Contact Us</div>
                    <div class="footer_latest_content">
                        <div class="footer_latest_item">
                            <!-- <div class="footer_latest_image"><img src="{{ url('assets/images/latest_2.jpg') }}" alt="https://unsplash.com/@sanfrancisco"></div> -->
                            <div class="footer_latest_item_content">
                                <div class="footer_latest_item_title"><a href="#"> <i class="fa fa-user" aria-hidden="true"></i> {{ $contactus_->namalengkap }}</a></div>
                                <div class="footer_latest_item_date"><a href="#"> <i class="fa fa-envelope" aria-hidden="true"></i> {{ $contactus_->email }}</a></div>
                                <div class="footer_latest_item_date"><a href="#"> <i class="fa fa-phone" aria-hidden="true"></i> {{ $contactus_->phone }}</a></div>
                                <div class="footer_latest_item_date"><a href="#"> <i class="fa fa-globe" aria-hidden="true"></i> {{ $contactus_->website }}</a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
</footer>