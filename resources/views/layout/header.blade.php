<?php
    $lang  	 = new \App\language();
    $lang_ 	 = $lang->getLanguage();

    $menu  	 = new \App\menu();
    $menu_ 	 = $menu->getMenu();

    $logo  	 = new \App\logo();
    $logo_ 	 = $logo->getLogo();

    $socialmedia  	 = new \App\socialmedia();
    $SocialMedia_ 	 = $socialmedia->getSocialMedia();
?>
<script type='text/css'>
    #bodyContent: {
        
    }
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<header class="header">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="header_container d-flex flex-row align-items-center justify-content-start">
                    <!-- Logo -->
                    <div class="logo_container">
                        <div class="logo">
                            <div>{{ $logo_->infoLogo }}</div>
                            <div></div>
                            <div class="logo_image"><img src="{{ url('assets/images/'.$logo_->logo) }}" alt=""></div>
                        </div>
                    </div>

                    <!-- Main Navigation -->
                    <nav class="main_nav ml-auto">
                        <div style="
                                top: 100px;
                                right: 0px;
                                top:0px;
                                position: absolute;
                                z-index: 99;
                                margin-bottom:10px;
                                padding: 5px;">
                            @foreach($lang_ as $a)
                                <a href="{{ url('locale/'.$a->language) }}" style="margin:5px;"> 
                                    <img src="{{ url('assets/images/'.$a->picture) }}" width="25px;" />
                                </a>
                            @endforeach
                        </div>
                        <ul class="main_nav_list">
                            @foreach($menu_ as $b)
                                @if($b->linkMenu === '#')
                                    <li class="main_nav_item"><a href="#" data-toggle="modal" data-target="#modalLoginForm">{{ $b->Menu }}</a></li>
                                @else
                                    <li class="main_nav_item"><a href="{{ $b->linkMenu }}">{{ $b->Menu }}</a></li>
                                @endif
                            @endforeach
                        </ul>
                    </nav>

                    <!-- Hamburger -->
                    <div class="hamburger ml-auto"><i class="fa fa-bars" aria-hidden="true"></i></div>

                </div>
            </div>
        </div>
    </div>
</header>
<div class="modal fade" id="modalLoginForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form method="post" action="{{ url('email/send') }}" >
                <div class="modal-header text-center">
                    <h4 class="modal-title w-100 font-weight-bold">@lang('lang.logintext')</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body mx-3">
                    <div class="md-form" style="margin-bottom:15px;">
                        <label data-error="wrong" data-success="right" for="defaultForm-email">Email</label>
                        <input type="email" id="defaultForm-email" name="email" class="form-control validate" placeholder="Email">
                    </div>

                    <div class="md-form">
                        <label data-error="wrong" data-success="right" for="defaultForm-pass">Password</label>
                        <input type="password" id="defaultForm-pass" name="password" class="form-control validate" placeholder="Password">
                    </div>

                    <div class="md-form" style="margin-top:15px;">
                        <button type='submit' class="btn btn-default">Login</button>
                        <a href="" style="margin-left:10px;">@lang('lang.forgotpass') </a>
                    </div>

                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <a href="#" class="btn btn-default close" data-dismiss="modal" style='width:100%;padding-top:10px;padding-bottom:10px;margin-right:17px;margin-left:17px;background:#000;color:#fff;font-size:15px;'  data-toggle="modal" data-target="#modalRegisForm">@lang('lang.register')</a>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="modalRegisForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form method="post" action="{{ url('email/send') }}" >
                <div class="modal-header text-center">
                    <h4 class="modal-title w-100 font-weight-bold">@lang('lang.register') @lang('lang.namewebsite')</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body mx-3">
                    <div class="md-form" style="margin-bottom:15px;">
                        <label data-error="wrong" data-success="right" for="defaultForm-email">@lang('lang.fullname')</label>
                        <input type="text" id="defaultForm-email" name="namalengkap" class="form-control validate" placeholder="@lang('lang.fullname')">
                    </div>

                    <div class="md-form" style="margin-bottom:15px;">
                        <label data-error="wrong" data-success="right" for="defaultForm-email">Email</label>
                        <input type="email" id="defaultForm-email" name="email" class="form-control validate" placeholder="Email">
                    </div>

                    <div class="md-form">
                        <label data-error="wrong" data-success="right" for="defaultForm-pass">Password</label>
                        <input type="password" id="defaultForm-pass" name="password" class="form-control validate" placeholder="Password">
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <button type='submit' class="btn btn-default" style='width:100%;margin-right:17px;margin-left:17px;'>@lang('lang.register')</button>
                </div>
                <!-- {{ csrf_token() }} -->
                @csrf
            </form>
        </div>
    </div>
</div>
<!-- Menu -->

<div class="menu_container menu_mm">

    <!-- Menu Close Button -->
    <div class="menu_close_container">
        <div class="menu_close"></div>
    </div>

    <!-- Menu Items -->
    <div class="menu_inner menu_mm">
        <div class="menu menu_mm">
            <ul class="menu_list menu_mm">
                @foreach($menu_ as $b)
                    @if($b->linkMenu === '#')
                        <li class="menu_item menu_mm"><a href="{{ $b->linkMenu }}" data-toggle="modal" data-target="#modalLoginForm">{{ $b->Menu }}</a></li>
                    @else
                        <li class="menu_item menu_mm"><a href="{{ $b->linkMenu }}">{{ $b->Menu }}</a></li>
                    @endif
                @endforeach
                @foreach($lang_ as $a)
                    <li class="menu_item menu_mm"><a href="locale/{{ $a->language }}">{{ $a->information }}</a></li>
                @endforeach
            </ul>

            <!-- Menu Social -->
            
            <div class="menu_social_container menu_mm">
                <ul class="menu_social menu_mm">
                    @foreach($SocialMedia_ as $c)
                        <li class="menu_social_item menu_mm"><a href="{{ $c->link }}"><i class="{{ $c->fontfa }}" aria-hidden="true"></i></a></li>
                    @endforeach
                </ul>
            </div>

            <div class="menu_copyright menu_mm">Colorlib All rights reserved</div>
        </div>
    </div>
</div>

