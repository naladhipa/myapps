<script src="{{ url('assets/js/jquery-3.2.1.min.js') }}"></script>
<script src="{{ url('assets/styles/bootstrap4/popper.js') }}"></script>
<script src="{{ url('assets/styles/bootstrap4/bootstrap.min.js') }}"></script>

<script src="{{ url('assets/plugins/magnific-popup/jquery.magnific-popup.min.js') }}"></script>
<script src="{{ url('assets/plugins/easing/easing.js') }}"></script>
<script src="{{ url('assets/plugins/parallax-js-master/parallax.min.js') }}"></script>
<script src="{{ url('assets/js/custom.js') }}"></script>

@yield('script')