<!DOCTYPE html>
<html>
<head>
    <title>@yield('title')</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Destino project">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSS -->
    @include('layout.css')
</head>
<body>

<div class="super_container">
	<!-- Header -->
	@include('layout.header')

	<!-- Home -->
    @yield('main')

	<!-- Footer -->
    @include('layout.footer')
</div>
<!-- JS -->
@include('layout.js')
</body>
</html>