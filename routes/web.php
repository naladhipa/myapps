<?php
Route::get('/', 'HomeController@index');
Route::get('/AboutMe', 'AboutController@index');
Route::get('/Destination/DetailDestination/{id}', 'DestinationController@DetailDestination');

Route::post('email/send','SendEmailController@send');

Route::get('locale/{locale}', function ($locale){
    Session::put('locale', $locale);
    return redirect()->back();
});